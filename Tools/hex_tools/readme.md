### hexinfo.exe 

* 为hex信息查看工具

```
hexinfo.exe out.hex
```



## mergehex.exe

* 为多个hex合并工具,最多只支持同时三个hex合并,输出仍然是hex

```
mergehex.exe -m a.hex b.hex c.hex -o out.hex
```

