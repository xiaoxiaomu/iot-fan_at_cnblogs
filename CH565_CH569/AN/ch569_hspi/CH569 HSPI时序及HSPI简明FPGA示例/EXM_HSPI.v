
module EXM_HSPI (
	//tx signal
	HTCLK,
	HTREQ,
	HTRDY,
	HTVLD,
	HTD,
	HTOE,
	tx_act,
	//rx signal
	HRCLK,
	HRACT,
	HRVLD,
	HTACK,
	HRD,
	//global signal
	rstn,
	clk,
	HSPI_MODE,
	dat_mod,
	//RAM signal
	ram_clk,
	ram_csn,
	ram_wen,
	ram_addr,
	ram_rdata,
	ram_wdata
);

output 	HTCLK;			//tx clock(max:120MHz)	
output 	HTREQ;			//tx request
input	HTRDY;			//tx ready
output	HTVLD;			//tx data valid
output 	[31:0] HTD;		//tx data
output 	HTOE;			//tx I/O output enable
input	tx_act;			//tx trigger

input	HRCLK;			//rx clock(max:120MHz)
input	HRACT;			//rx active
input	HRVLD;			//rx data valid
output	HTACK;			//ack to transmitter
input 	[31:0] HRD;		//rx data

input	rstn;			//global reset signal, low action
input	clk;			//global clock
input	HSPI_MODE;		//HSPI mode, 1 - up side, 0 - down side
input 	[1:0] dat_mod;	//HSPI data mode, 00 - 8bits, 01 - 16bits, 1x - 32bits

output	ram_clk;		//ram clock
output	ram_csn;		//ram chip-select, low action
output	ram_wen;		//ram write enable, low action
output	[8:0] ram_addr;		//ram address
output	[31:0] ram_wdata;	//ram write data
input	[31:0] ram_rdata;	//ram read data

parameter	TX_LEN = 12'd512 - 1;			//tx length
parameter	HSPI_UDF = 26'h00F0F0F;			//user define filed
parameter	TX_ADDR = 9'h000;				//address of tx data 
parameter	RX_ADDR = 9'h100;				//address of rx data

parameter 	IDLE = 2'b00;
parameter 	WAIT_RDY = 2'b01;
parameter 	TRAN_HD = 2'b10;
parameter 	TRAN_DAT = 2'b11;

//signal define
wire	wire_rst;
reg		reg_reset;
reg		reg_reset_dly;
wire	wire_reset_sync;
reg		reg_tx_ack;
reg		reg_tran_req;
wire	wire_tran_dat_end;
reg		reg_tx_rdy;
reg		reg_tx_rdy_dly;
reg		reg_hd_dly;
wire	wire_st_tran_hd;
wire	wire_st_tran_dat;
wire	wire_tran_hd_entry;
wire	wire_tran_dat_entry;
reg 	[1:0] reg_state;
wire	wire_end_32b;
reg 	[11:0] reg_tran_cnt;
reg		reg_pif_tran_period;
reg		reg_tran_dat_end_d1;
wire	wire_crc_sel;
wire	wire_tran_hd_sel;
wire 	[31:0] wire_tran_hd_dat;
reg 	[3:0] reg_tran_num;
reg 	[31:0] wire_send_data;
wire 	[31:0] wire_tx_crc_res32;
reg		reg_tx_req_dly;
reg		reg_tx_vld_dly;
reg		[31:0] reg_tx_dat_dly;	
wire	wire_tx_crc_clr;
wire 	wire_tx_crc_en;
wire 	[31:0] wire_tx_crc_in;
wire 	[31:0] wire_tx_crc_out32;
reg		reg_rx_active;
reg		reg_rx_act_d1;
reg		reg_rx_valid;
reg 	[31:0] reg_rx_data;
wire	wire_rx_end;
reg		reg_rxv1;
reg 	[31:0] reg_ram_dat;
reg		reg_rx_data_period;
reg 	[31:0] reg_recv_udf;
wire	wire_rx_crc_clr;
wire 	[31:0] wire_recv_crc_in;
wire	wire_dat32_mod;
wire	wire_recv_check;
reg		reg_rx_crc_err;
wire 	[31:0] wire_recv_crc_out32;
wire 	[15:0] wire_recv_crc_res;
reg		reg_num_mismatch;
reg 	[3:0] reg_recv_num;
reg		reg_recv_check;
wire	wire_crc_en;
wire	wire_crc_clr;
wire	wire_crc_clk;
wire 	[31:0] wire_crc_in;	
wire 	[31:0] wire_crc_out32;
reg		reg_rx_active_sync;

assign wire_rst = ~rstn;
always @(posedge HRCLK or posedge wire_rst) begin	
	if (wire_rst) begin
		reg_reset <= 1'b1;
		reg_reset_dly <= 1'b1;
	end
	else begin
		reg_reset <= 1'b0;
		reg_reset_dly <= reg_reset;
	end
end

assign wire_reset_sync = reg_reset_dly;

//************* tx logic ***************
always @(posedge clk) reg_rx_active_sync <= HRACT;		//sync HRACT

always @(posedge clk or posedge wire_rst) begin
	if (wire_rst) reg_tx_ack <= 1'b0;
	else if (reg_rx_active_sync) begin
		if (reg_tran_req) reg_tx_ack <= ~HSPI_MODE;		//up side first
		else reg_tx_ack <= 1'b1;
	end
	else reg_tx_ack <= 1'b0;
end		
	
assign HTACK = reg_tx_ack;

always @(posedge clk or posedge wire_rst) begin		//tx request
	if (wire_rst) reg_tran_req <= 1'b0;
	else if (wire_tran_dat_end) reg_tran_req <= 1'b0;
	else if (tx_act & !reg_rx_active_sync) reg_tran_req <= 1'b1;
end

always @(posedge clk) begin
	reg_tx_rdy <= HTRDY;					//sync tx ready signal
	reg_tx_rdy_dly <= reg_tx_rdy;			//dly
end

always @(posedge clk) reg_hd_dly <= wire_st_tran_hd;

assign wire_tran_hd_sel = wire_st_tran_hd & !reg_hd_dly;		//packet header tx
assign wire_tran_hd_entry = reg_tx_rdy & !reg_tx_rdy_dly & reg_state == WAIT_RDY;	//tx header entry
assign wire_tran_dat_entry = wire_tran_hd_sel & reg_state == TRAN_HD;	//tx data entry

always @(posedge clk or posedge wire_rst) begin		//tx FSM
	if (wire_rst) reg_state <= IDLE;	//default IDLE
	else begin
		case(reg_state)
			IDLE: if (tx_act) reg_state <= WAIT_RDY;					//wait tirgger
			WAIT_RDY: if (wire_tran_hd_entry) reg_state <= TRAN_HD;		//keep wait until tx ready
			TRAN_HD:  if (wire_tran_dat_entry) reg_state <= TRAN_DAT;	//tx packet header
			TRAN_DAT: if (wire_tran_dat_end) reg_state <= IDLE;			//tx data payload
			default: reg_state <= IDLE;
		endcase
	end
end

assign wire_st_tran_hd = reg_state == TRAN_HD;
assign wire_st_tran_dat = reg_state == TRAN_DAT;

always @(posedge clk or posedge wire_rst) begin		//tx count
	if (wire_rst) reg_tran_cnt <= 12'd0;
	else if (wire_tran_dat_entry) reg_tran_cnt <= 12'd0;	//clear when new data tx begin
	else if (wire_st_tran_dat) reg_tran_cnt <= reg_tran_cnt + 1'b1;		//keep increase during data tx state
end

assign wire_end_32b = reg_tran_cnt == TX_LEN[11:2];		//32bits mode data end
assign wire_tran_dat_end = wire_st_tran_dat & wire_end_32b;			//data payload tx end

always @(posedge clk or posedge wire_rst) begin		//tx period
	if (wire_rst) reg_pif_tran_period <= 1'b0;
	else if (reg_tx_rdy & !reg_tx_rdy_dly) reg_pif_tran_period <= 1'b1;
	else if (wire_crc_sel) reg_pif_tran_period <= 1'b0;
end

always @(posedge clk) reg_tran_dat_end_d1 <= wire_tran_dat_end;
assign wire_crc_sel = reg_tran_dat_end_d1;

assign wire_tran_hd_dat = {TX_LEN[1:0], reg_tran_num, HSPI_UDF};	//content of header
always @(*) begin	//tx data select
	if (wire_tran_hd_sel) wire_send_data = wire_tran_hd_dat;		//tx UDF
	else if (wire_crc_sel) wire_send_data = wire_tx_crc_res32;		//tx CRC
	else wire_send_data = ram_rdata;								//tx data payload
end

always @(posedge clk or posedge wire_rst) begin		//tx sequence count
	if (wire_rst) reg_tran_num <= 4'd0;
	else if (wire_tran_dat_end) reg_tran_num <= reg_tran_num + 1'b1;	//increase when tx done
end

always @(posedge clk) begin
	reg_tx_req_dly <= reg_tran_req | wire_crc_sel;
	reg_tx_vld_dly <= wire_st_tran_hd | wire_st_tran_dat | wire_crc_sel;
	reg_tx_dat_dly <= wire_send_data;
end

//tx interface signal
assign #0.2 HTREQ = reg_tx_req_dly;
assign #0.2 HTD = reg_tx_dat_dly;
assign #0.2 HTVLD = reg_tx_vld_dly;
assign #0.2 HTOE = reg_tx_vld_dly;	
assign #0.1 HTCLK = ~clk;


//tx crc
assign wire_tx_crc_clr = wire_rst | wire_tran_hd_entry;		//CRC module reset
assign wire_tx_crc_en = wire_st_tran_dat | wire_st_tran_hd;		//calculate CRC during header and data payload tx period
assign wire_tx_crc_in  = {wire_send_data[0], wire_send_data[1], wire_send_data[2], wire_send_data[3],
						  wire_send_data[4], wire_send_data[5], wire_send_data[6], wire_send_data[7],
						  wire_send_data[8], wire_send_data[9], wire_send_data[10], wire_send_data[11],
						  wire_send_data[12], wire_send_data[13], wire_send_data[14], wire_send_data[15],
						  wire_send_data[16], wire_send_data[17], wire_send_data[18], wire_send_data[19],
						  wire_send_data[20], wire_send_data[21], wire_send_data[22], wire_send_data[23],
						  wire_send_data[24], wire_send_data[25], wire_send_data[26], wire_send_data[27],
						  wire_send_data[28], wire_send_data[29], wire_send_data[30], wire_send_data[31]
						  };

assign wire_tx_crc_res32 = {~wire_tx_crc_out32[0], ~wire_tx_crc_out32[1], ~wire_tx_crc_out32[2], ~wire_tx_crc_out32[3], 
						    ~wire_tx_crc_out32[4], ~wire_tx_crc_out32[5], ~wire_tx_crc_out32[6], ~wire_tx_crc_out32[7],
						    ~wire_tx_crc_out32[8], ~wire_tx_crc_out32[9], ~wire_tx_crc_out32[10], ~wire_tx_crc_out32[11], 
						    ~wire_tx_crc_out32[12], ~wire_tx_crc_out32[13], ~wire_tx_crc_out32[14], ~wire_tx_crc_out32[15], 
						    ~wire_tx_crc_out32[16], ~wire_tx_crc_out32[17], ~wire_tx_crc_out32[18], ~wire_tx_crc_out32[19],
						    ~wire_tx_crc_out32[20], ~wire_tx_crc_out32[21], ~wire_tx_crc_out32[22], ~wire_tx_crc_out32[23],
						    ~wire_tx_crc_out32[24], ~wire_tx_crc_out32[25], ~wire_tx_crc_out32[26], ~wire_tx_crc_out32[27],
						    ~wire_tx_crc_out32[28], ~wire_tx_crc_out32[29], ~wire_tx_crc_out32[30], ~wire_tx_crc_out32[31]
						   };


//************* rx logic ***************
always @(posedge HRCLK) begin
	reg_rx_active <= HRACT;		
	reg_rx_act_d1 <= reg_rx_active;
	reg_rx_valid <= HRVLD;		
	reg_rx_data <= HRD;
end

assign wire_rx_end = !HRACT & reg_rx_active;		//rx done when HRACT falling
always @(posedge HRCLK or posedge wire_reset_sync) begin	//data active period
	if (wire_reset_sync) reg_rxv1 <= 1'b0;
	else if (reg_rx_valid) reg_rxv1 <= 1'b1;
	else if (wire_rx_end) reg_rxv1 <= 1'b0;
end

always @(posedge HRCLK) begin	//save data when both HRACT and HRVLD are active
	if (HRACT & reg_rxv1 & reg_rx_valid) reg_ram_dat <= reg_rx_data;		//not include UDF and CRC
end

always @(posedge HRCLK or posedge wire_reset_sync) begin		//receive data period
	if (wire_reset_sync) reg_rx_data_period <= 1'b0;
	else if (!reg_rx_data_period & reg_rx_valid) reg_rx_data_period <= 1'b1;
	else if (wire_rx_end) reg_rx_data_period <= 1'b0;
end

always @(posedge HRCLK or posedge wire_reset_sync) begin	//rx UDF
	if (wire_reset_sync) reg_recv_udf <= 32'd0;
	else if (reg_rx_valid & !reg_rx_data_period) reg_recv_udf <= reg_rx_data[31:0];
end

//rx crc
assign wire_rx_crc_clr = wire_reset_sync | HRACT & !reg_rx_active;		//rx crc module reset
assign wire_recv_crc_in = {reg_rx_data[0], reg_rx_data[1], reg_rx_data[2], reg_rx_data[3],
						   reg_rx_data[4], reg_rx_data[5], reg_rx_data[6], reg_rx_data[7],
						   reg_rx_data[8], reg_rx_data[9], reg_rx_data[10], reg_rx_data[11],
						   reg_rx_data[12], reg_rx_data[13], reg_rx_data[14], reg_rx_data[15],
						   reg_rx_data[16], reg_rx_data[17], reg_rx_data[18], reg_rx_data[19],
						   reg_rx_data[20], reg_rx_data[21], reg_rx_data[22], reg_rx_data[23],
						   reg_rx_data[24], reg_rx_data[25], reg_rx_data[26], reg_rx_data[27],
						   reg_rx_data[28], reg_rx_data[29], reg_rx_data[30], reg_rx_data[31]					   
						  };

assign wire_dat32_mod = dat_mod[1] == 1'b1;		//dat_mod = 2'b1x -> 32bits mode
assign wire_recv_check = !reg_rx_active & reg_rx_act_d1;	//rx status check
always @(posedge HRCLK or posedge wire_reset_sync) begin
	if (wire_reset_sync) reg_rx_crc_err <= 1'b0;
	else if (wire_rx_end) reg_rx_crc_err <= 1'b0;	//clear last CRC flag before new CRC result coming
	else if (wire_recv_check) begin
		if (wire_dat32_mod) reg_rx_crc_err <= wire_recv_crc_out32 != 32'hC704DD7B;	//32bit CRC check
		else reg_rx_crc_err <= wire_recv_crc_res != 16'h800d;	//8bit/16bits CRC check
	end
end

always @(posedge HRCLK or posedge wire_reset_sync) begin	//tx and rx sequence match check
	if (wire_reset_sync) reg_num_mismatch <= 1'b0;
	else if (wire_recv_check) reg_num_mismatch <= reg_recv_num != reg_recv_udf[29:26];
end

always @(posedge HRCLK) reg_recv_check <= wire_recv_check;
always @(posedge HRCLK or posedge wire_reset_sync) begin	//rx sequence count
	if (wire_reset_sync) reg_recv_num <= 4'd0;
	else if (reg_recv_check & !reg_num_mismatch & !reg_rx_crc_err) reg_recv_num <= reg_recv_num + 1'b1;
end

//************* crc logic ***************
assign wire_crc_en = reg_pif_tran_period ? wire_tx_crc_en : reg_rx_valid;		//CRC calculate enable
assign wire_crc_clr = reg_pif_tran_period ? wire_tx_crc_clr : wire_rx_crc_clr;	//CRC reset
assign wire_crc_clk = reg_pif_tran_period ? clk : HRCLK;						//CRC clock
assign wire_crc_in = reg_pif_tran_period ? wire_tx_crc_in : wire_recv_crc_in;	//CRC datain
assign wire_tx_crc_out32 = wire_crc_out32;										//tx CRC32 result
assign wire_recv_crc_out32 = wire_crc_out32;									//rx CRC32 result

crc32_32b m_crc32 (
	.clk(wire_crc_clk),
	.rst(wire_crc_clr),
	.crc_en(wire_crc_en),
	.data_in(wire_crc_in),
	.crc_out(wire_crc_out32)
);

//RAM operation
reg		reg_ram_cs_tx;
always @(posedge clk or posedge wire_rst) begin		//ram cs during tx
	if (wire_rst) reg_ram_cs_tx <= 1'b0;
	else begin
		if (wire_tran_hd_entry) reg_ram_cs_tx <= 1'b1;
		else if (wire_tran_dat_end) reg_ram_cs_tx <= 1'b0;
	end
end

reg		reg_ram_cs_rx;
always @(posedge HRCLK or posedge wire_reset_sync) begin	//ram cs during rx
	if (wire_reset_sync) reg_ram_cs_rx <= 1'b0;
	else reg_ram_cs_rx <= HRACT & reg_rxv1 & reg_rx_valid;
end

reg		[8:0] reg_ram_addr_tx;
always @(posedge clk or posedge wire_rst) begin		//ram read address
	if (wire_rst) reg_ram_addr_tx <= 9'd0;
	else if (wire_tran_hd_entry) reg_ram_addr_tx <= TX_ADDR;
	else if (wire_st_tran_hd | wire_st_tran_dat & !wire_tran_dat_end) reg_ram_addr_tx <= reg_ram_addr_tx + 1'b1;
end

reg		[8:0] reg_ram_addr_rx;
always @(posedge HRCLK or posedge wire_reset_sync) begin	//ram write address
	if (wire_reset_sync) reg_ram_addr_rx <= 9'd0;
	else if (HRVLD & !reg_rx_valid) reg_ram_addr_rx <= RX_ADDR;
	else if (reg_ram_cs_rx) reg_ram_addr_rx <= reg_ram_addr_rx + 1'b1;
end

//note:
// 1. make sure that ram_csn inactive when switch ram clock
// 2. latch can be used to avoid glitch during clock switch
assign ram_clk = reg_tx_rdy ? clk : HRCLK;			
assign ram_csn = reg_tx_rdy ? !(reg_ram_cs_tx & !wire_tran_dat_end) : !reg_ram_cs_rx;
assign ram_wen = !reg_ram_cs_rx;
assign ram_addr = reg_tx_rdy ? reg_ram_addr_tx : reg_ram_addr_rx;
assign ram_wdata = reg_ram_dat;


endmodule

module crc16_8b(				//crc[15:0]=1+x^2+x^15+x^16;
  input [7:0] data_in,
  input crc_en,
  output [15:0] crc_out,
  input rst,
  input clk);

  reg [15:0] lfsr_q,lfsr_c;

  assign crc_out = lfsr_q;

  always @(*) begin
    lfsr_c[0] = lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[15] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7];
    lfsr_c[1] = lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[15] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7];
    lfsr_c[2] = lfsr_q[8] ^ lfsr_q[9] ^ data_in[0] ^ data_in[1];
    lfsr_c[3] = lfsr_q[9] ^ lfsr_q[10] ^ data_in[1] ^ data_in[2];
    lfsr_c[4] = lfsr_q[10] ^ lfsr_q[11] ^ data_in[2] ^ data_in[3];
    lfsr_c[5] = lfsr_q[11] ^ lfsr_q[12] ^ data_in[3] ^ data_in[4];
    lfsr_c[6] = lfsr_q[12] ^ lfsr_q[13] ^ data_in[4] ^ data_in[5];
    lfsr_c[7] = lfsr_q[13] ^ lfsr_q[14] ^ data_in[5] ^ data_in[6];
    lfsr_c[8] = lfsr_q[0] ^ lfsr_q[14] ^ lfsr_q[15] ^ data_in[6] ^ data_in[7];
    lfsr_c[9] = lfsr_q[1] ^ lfsr_q[15] ^ data_in[7];
    lfsr_c[10] = lfsr_q[2];
    lfsr_c[11] = lfsr_q[3];
    lfsr_c[12] = lfsr_q[4];
    lfsr_c[13] = lfsr_q[5];
    lfsr_c[14] = lfsr_q[6];
    lfsr_c[15] = lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[15] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7];

  end // always

  always @(posedge clk, posedge rst) begin
    if(rst) begin
      lfsr_q <= {16{1'b1}};
    end
    else begin
      lfsr_q <= crc_en ? lfsr_c : lfsr_q;
    end
  end // always
endmodule // crc

module crc16_16b(				//crc[15:0]=1+x^2+x^15+x^16;
  input [15:0] data_in,
  input crc_en,
  output [15:0] crc_out,
  input rst,
  input clk);

  reg [15:0] lfsr_q,lfsr_c;

  assign crc_out = lfsr_q;

  always @(*) begin
    lfsr_c[0] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[15] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[13] ^ data_in[15];
    lfsr_c[1] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[14] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[13] ^ data_in[14];
    lfsr_c[2] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[14] ^ data_in[0] ^ data_in[1] ^ data_in[14];
    lfsr_c[3] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[15] ^ data_in[1] ^ data_in[2] ^ data_in[15];
    lfsr_c[4] = lfsr_q[2] ^ lfsr_q[3] ^ data_in[2] ^ data_in[3];
    lfsr_c[5] = lfsr_q[3] ^ lfsr_q[4] ^ data_in[3] ^ data_in[4];
    lfsr_c[6] = lfsr_q[4] ^ lfsr_q[5] ^ data_in[4] ^ data_in[5];
    lfsr_c[7] = lfsr_q[5] ^ lfsr_q[6] ^ data_in[5] ^ data_in[6];
    lfsr_c[8] = lfsr_q[6] ^ lfsr_q[7] ^ data_in[6] ^ data_in[7];
    lfsr_c[9] = lfsr_q[7] ^ lfsr_q[8] ^ data_in[7] ^ data_in[8];
    lfsr_c[10] = lfsr_q[8] ^ lfsr_q[9] ^ data_in[8] ^ data_in[9];
    lfsr_c[11] = lfsr_q[9] ^ lfsr_q[10] ^ data_in[9] ^ data_in[10];
    lfsr_c[12] = lfsr_q[10] ^ lfsr_q[11] ^ data_in[10] ^ data_in[11];
    lfsr_c[13] = lfsr_q[11] ^ lfsr_q[12] ^ data_in[11] ^ data_in[12];
    lfsr_c[14] = lfsr_q[12] ^ lfsr_q[13] ^ data_in[12] ^ data_in[13];
    lfsr_c[15] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[14] ^ lfsr_q[15] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[14] ^ data_in[15];

  end // always

  always @(posedge clk, posedge rst) begin
    if(rst) begin
      lfsr_q <= {16{1'b1}};
    end
    else begin
      lfsr_q <= crc_en ? lfsr_c : lfsr_q;
    end
  end // always
endmodule // crc

module crc32_32b(			//crc[31:0] = 1 + X + X^2 + X^4 + X^5 + X^7 + X^8 + X^10 + X^11 + X^12 + X^16 + X^22 + X^23 + X^26 + X^32
	input	clk,
	input	rst,
	input 	[31:0]	data_in,
	input	crc_en,
	output	[31:0]	crc_out
);

	wire  [31:0] lfsr_c;
	reg	 [31:0] lfsr_q;
	
	assign crc_out = lfsr_q;
	
	assign	lfsr_c[0] = lfsr_q[0] ^ lfsr_q[6] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[12] ^ lfsr_q[16] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[26] ^ lfsr_q[28] ^ lfsr_q[29] ^ lfsr_q[30] ^ lfsr_q[31] ^ data_in[0] ^ data_in[6] ^ data_in[9] ^ data_in[10] ^ data_in[12] ^ data_in[16] ^ data_in[24] ^ data_in[25] ^ data_in[26] ^ data_in[28] ^ data_in[29] ^ data_in[30] ^ data_in[31];
	assign	lfsr_c[1] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[9] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[16] ^ lfsr_q[17] ^ lfsr_q[24] ^ lfsr_q[27] ^ lfsr_q[28] ^ data_in[0] ^ data_in[1] ^ data_in[6] ^ data_in[7] ^ data_in[9] ^ data_in[11] ^ data_in[12] ^ data_in[13] ^ data_in[16] ^ data_in[17] ^ data_in[24] ^ data_in[27] ^ data_in[28];
	assign	lfsr_c[2] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[16] ^ lfsr_q[17] ^ lfsr_q[18] ^ lfsr_q[24] ^ lfsr_q[26] ^ lfsr_q[30] ^ lfsr_q[31] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[13] ^ data_in[14] ^ data_in[16] ^ data_in[17] ^ data_in[18] ^ data_in[24] ^ data_in[26] ^ data_in[30] ^ data_in[31];
	assign	lfsr_c[3] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[14] ^ lfsr_q[15] ^ lfsr_q[17] ^ lfsr_q[18] ^ lfsr_q[19] ^ lfsr_q[25] ^ lfsr_q[27] ^ lfsr_q[31] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[14] ^ data_in[15] ^ data_in[17] ^ data_in[18] ^ data_in[19] ^ data_in[25] ^ data_in[27] ^ data_in[31];
	assign	lfsr_c[4] = lfsr_q[0] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[6] ^ lfsr_q[8] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[15] ^ lfsr_q[18] ^ lfsr_q[19] ^ lfsr_q[20] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[29] ^ lfsr_q[30] ^ lfsr_q[31] ^ data_in[0] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[8] ^ data_in[11] ^ data_in[12] ^ data_in[15] ^ data_in[18] ^ data_in[19] ^ data_in[20] ^ data_in[24] ^ data_in[25] ^ data_in[29] ^ data_in[30] ^ data_in[31];
	assign	lfsr_c[5] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[10] ^ lfsr_q[13] ^ lfsr_q[19] ^ lfsr_q[20] ^ lfsr_q[21] ^ lfsr_q[24] ^ lfsr_q[28] ^ lfsr_q[29] ^ data_in[0] ^ data_in[1] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[10] ^ data_in[13] ^ data_in[19] ^ data_in[20] ^ data_in[21] ^ data_in[24] ^ data_in[28] ^ data_in[29];
	assign	lfsr_c[6] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[11] ^ lfsr_q[14] ^ lfsr_q[20] ^ lfsr_q[21] ^ lfsr_q[22] ^ lfsr_q[25] ^ lfsr_q[29] ^ lfsr_q[30] ^ data_in[1] ^ data_in[2] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[11] ^ data_in[14] ^ data_in[20] ^ data_in[21] ^ data_in[22] ^ data_in[25] ^ data_in[29] ^ data_in[30];
	assign	lfsr_c[7] = lfsr_q[0] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[5] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[10] ^ lfsr_q[15] ^ lfsr_q[16] ^ lfsr_q[21] ^ lfsr_q[22] ^ lfsr_q[23] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[28] ^ lfsr_q[29] ^ data_in[0] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[7] ^ data_in[8] ^ data_in[10] ^ data_in[15] ^ data_in[16] ^ data_in[21] ^ data_in[22] ^ data_in[23] ^ data_in[24] ^ data_in[25] ^ data_in[28] ^ data_in[29];
	assign	lfsr_c[8] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[8] ^ lfsr_q[10] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[17] ^ lfsr_q[22] ^ lfsr_q[23] ^ lfsr_q[28] ^ lfsr_q[31] ^ data_in[0] ^ data_in[1] ^ data_in[3] ^ data_in[4] ^ data_in[8] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[17] ^ data_in[22] ^ data_in[23] ^ data_in[28] ^ data_in[31];
	assign	lfsr_c[9] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[9] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[18] ^ lfsr_q[23] ^ lfsr_q[24] ^ lfsr_q[29] ^ data_in[1] ^ data_in[2] ^ data_in[4] ^ data_in[5] ^ data_in[9] ^ data_in[11] ^ data_in[12] ^ data_in[13] ^ data_in[18] ^ data_in[23] ^ data_in[24] ^ data_in[29];
	assign	lfsr_c[10] = lfsr_q[0] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[5] ^ lfsr_q[9] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[16] ^ lfsr_q[19] ^ lfsr_q[26] ^ lfsr_q[28] ^ lfsr_q[29] ^ lfsr_q[31] ^ data_in[0] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[9] ^ data_in[13] ^ data_in[14] ^ data_in[16] ^ data_in[19] ^ data_in[26] ^ data_in[28] ^ data_in[29] ^ data_in[31];
	assign	lfsr_c[11] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[9] ^ lfsr_q[12] ^ lfsr_q[14] ^ lfsr_q[15] ^ lfsr_q[16] ^ lfsr_q[17] ^ lfsr_q[20] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[26] ^ lfsr_q[27] ^ lfsr_q[28] ^ lfsr_q[31] ^ data_in[0] ^ data_in[1] ^ data_in[3] ^ data_in[4] ^ data_in[9] ^ data_in[12] ^ data_in[14] ^ data_in[15] ^ data_in[16] ^ data_in[17] ^ data_in[20] ^ data_in[24] ^ data_in[25] ^ data_in[26] ^ data_in[27] ^ data_in[28] ^ data_in[31];
	assign	lfsr_c[12] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[9] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[15] ^ lfsr_q[17] ^ lfsr_q[18] ^ lfsr_q[21] ^ lfsr_q[24] ^ lfsr_q[27] ^ lfsr_q[30] ^ lfsr_q[31] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[9] ^ data_in[12] ^ data_in[13] ^ data_in[15] ^ data_in[17] ^ data_in[18] ^ data_in[21] ^ data_in[24] ^ data_in[27] ^ data_in[30] ^ data_in[31];
	assign	lfsr_c[13] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[10] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[16] ^ lfsr_q[18] ^ lfsr_q[19] ^ lfsr_q[22] ^ lfsr_q[25] ^ lfsr_q[28] ^ lfsr_q[31] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[10] ^ data_in[13] ^ data_in[14] ^ data_in[16] ^ data_in[18] ^ data_in[19] ^ data_in[22] ^ data_in[25] ^ data_in[28] ^ data_in[31];
	assign	lfsr_c[14] = lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[11] ^ lfsr_q[14] ^ lfsr_q[15] ^ lfsr_q[17] ^ lfsr_q[19] ^ lfsr_q[20] ^ lfsr_q[23] ^ lfsr_q[26] ^ lfsr_q[29] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[11] ^ data_in[14] ^ data_in[15] ^ data_in[17] ^ data_in[19] ^ data_in[20] ^ data_in[23] ^ data_in[26] ^ data_in[29];
	assign	lfsr_c[15] = lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[12] ^ lfsr_q[15] ^ lfsr_q[16] ^ lfsr_q[18] ^ lfsr_q[20] ^ lfsr_q[21] ^ lfsr_q[24] ^ lfsr_q[27] ^ lfsr_q[30] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[12] ^ data_in[15] ^ data_in[16] ^ data_in[18] ^ data_in[20] ^ data_in[21] ^ data_in[24] ^ data_in[27] ^ data_in[30];
	assign	lfsr_c[16] = lfsr_q[0] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[8] ^ lfsr_q[12] ^ lfsr_q[13] ^ lfsr_q[17] ^ lfsr_q[19] ^ lfsr_q[21] ^ lfsr_q[22] ^ lfsr_q[24] ^ lfsr_q[26] ^ lfsr_q[29] ^ lfsr_q[30] ^ data_in[0] ^ data_in[4] ^ data_in[5] ^ data_in[8] ^ data_in[12] ^ data_in[13] ^ data_in[17] ^ data_in[19] ^ data_in[21] ^ data_in[22] ^ data_in[24] ^ data_in[26] ^ data_in[29] ^ data_in[30];
	assign	lfsr_c[17] = lfsr_q[1] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[9] ^ lfsr_q[13] ^ lfsr_q[14] ^ lfsr_q[18] ^ lfsr_q[20] ^ lfsr_q[22] ^ lfsr_q[23] ^ lfsr_q[25] ^ lfsr_q[27] ^ lfsr_q[30] ^ lfsr_q[31] ^ data_in[1] ^ data_in[5] ^ data_in[6] ^ data_in[9] ^ data_in[13] ^ data_in[14] ^ data_in[18] ^ data_in[20] ^ data_in[22] ^ data_in[23] ^ data_in[25] ^ data_in[27] ^ data_in[30] ^ data_in[31];
	assign	lfsr_c[18] = lfsr_q[2] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[10] ^ lfsr_q[14] ^ lfsr_q[15] ^ lfsr_q[19] ^ lfsr_q[21] ^ lfsr_q[23] ^ lfsr_q[24] ^ lfsr_q[26] ^ lfsr_q[28] ^ lfsr_q[31] ^ data_in[2] ^ data_in[6] ^ data_in[7] ^ data_in[10] ^ data_in[14] ^ data_in[15] ^ data_in[19] ^ data_in[21] ^ data_in[23] ^ data_in[24] ^ data_in[26] ^ data_in[28] ^ data_in[31];
	assign	lfsr_c[19] = lfsr_q[3] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[11] ^ lfsr_q[15] ^ lfsr_q[16] ^ lfsr_q[20] ^ lfsr_q[22] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[27] ^ lfsr_q[29] ^ data_in[3] ^ data_in[7] ^ data_in[8] ^ data_in[11] ^ data_in[15] ^ data_in[16] ^ data_in[20] ^ data_in[22] ^ data_in[24] ^ data_in[25] ^ data_in[27] ^ data_in[29];
	assign	lfsr_c[20] = lfsr_q[4] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[12] ^ lfsr_q[16] ^ lfsr_q[17] ^ lfsr_q[21] ^ lfsr_q[23] ^ lfsr_q[25] ^ lfsr_q[26] ^ lfsr_q[28] ^ lfsr_q[30] ^ data_in[4] ^ data_in[8] ^ data_in[9] ^ data_in[12] ^ data_in[16] ^ data_in[17] ^ data_in[21] ^ data_in[23] ^ data_in[25] ^ data_in[26] ^ data_in[28] ^ data_in[30];
	assign	lfsr_c[21] = lfsr_q[5] ^ lfsr_q[9] ^ lfsr_q[10] ^ lfsr_q[13] ^ lfsr_q[17] ^ lfsr_q[18] ^ lfsr_q[22] ^ lfsr_q[24] ^ lfsr_q[26] ^ lfsr_q[27] ^ lfsr_q[29] ^ lfsr_q[31] ^ data_in[5] ^ data_in[9] ^ data_in[10] ^ data_in[13] ^ data_in[17] ^ data_in[18] ^ data_in[22] ^ data_in[24] ^ data_in[26] ^ data_in[27] ^ data_in[29] ^ data_in[31];
	assign	lfsr_c[22] = lfsr_q[0] ^ lfsr_q[9] ^ lfsr_q[11] ^ lfsr_q[12] ^ lfsr_q[14] ^ lfsr_q[16] ^ lfsr_q[18] ^ lfsr_q[19] ^ lfsr_q[23] ^ lfsr_q[24] ^ lfsr_q[26] ^ lfsr_q[27] ^ lfsr_q[29] ^ lfsr_q[31] ^ data_in[0] ^ data_in[9] ^ data_in[11] ^ data_in[12] ^ data_in[14] ^ data_in[16] ^ data_in[18] ^ data_in[19] ^ data_in[23] ^ data_in[24] ^ data_in[26] ^ data_in[27] ^ data_in[29] ^ data_in[31];
	assign	lfsr_c[23] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[6] ^ lfsr_q[9] ^ lfsr_q[13] ^ lfsr_q[15] ^ lfsr_q[16] ^ lfsr_q[17] ^ lfsr_q[19] ^ lfsr_q[20] ^ lfsr_q[26] ^ lfsr_q[27] ^ lfsr_q[29] ^ lfsr_q[31] ^ data_in[0] ^ data_in[1] ^ data_in[6] ^ data_in[9] ^ data_in[13] ^ data_in[15] ^ data_in[16] ^ data_in[17] ^ data_in[19] ^ data_in[20] ^ data_in[26] ^ data_in[27] ^ data_in[29] ^ data_in[31];
	assign	lfsr_c[24] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[7] ^ lfsr_q[10] ^ lfsr_q[14] ^ lfsr_q[16] ^ lfsr_q[17] ^ lfsr_q[18] ^ lfsr_q[20] ^ lfsr_q[21] ^ lfsr_q[27] ^ lfsr_q[28] ^ lfsr_q[30] ^ data_in[1] ^ data_in[2] ^ data_in[7] ^ data_in[10] ^ data_in[14] ^ data_in[16] ^ data_in[17] ^ data_in[18] ^ data_in[20] ^ data_in[21] ^ data_in[27] ^ data_in[28] ^ data_in[30];
	assign	lfsr_c[25] = lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[8] ^ lfsr_q[11] ^ lfsr_q[15] ^ lfsr_q[17] ^ lfsr_q[18] ^ lfsr_q[19] ^ lfsr_q[21] ^ lfsr_q[22] ^ lfsr_q[28] ^ lfsr_q[29] ^ lfsr_q[31] ^ data_in[2] ^ data_in[3] ^ data_in[8] ^ data_in[11] ^ data_in[15] ^ data_in[17] ^ data_in[18] ^ data_in[19] ^ data_in[21] ^ data_in[22] ^ data_in[28] ^ data_in[29] ^ data_in[31];
	assign	lfsr_c[26] = lfsr_q[0] ^ lfsr_q[3] ^ lfsr_q[4] ^ lfsr_q[6] ^ lfsr_q[10] ^ lfsr_q[18] ^ lfsr_q[19] ^ lfsr_q[20] ^ lfsr_q[22] ^ lfsr_q[23] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[26] ^ lfsr_q[28] ^ lfsr_q[31] ^ data_in[0] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[10] ^ data_in[18] ^ data_in[19] ^ data_in[20] ^ data_in[22] ^ data_in[23] ^ data_in[24] ^ data_in[25] ^ data_in[26] ^ data_in[28] ^ data_in[31];
	assign	lfsr_c[27] = lfsr_q[1] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[7] ^ lfsr_q[11] ^ lfsr_q[19] ^ lfsr_q[20] ^ lfsr_q[21] ^ lfsr_q[23] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[26] ^ lfsr_q[27] ^ lfsr_q[29] ^ data_in[1] ^ data_in[4] ^ data_in[5] ^ data_in[7] ^ data_in[11] ^ data_in[19] ^ data_in[20] ^ data_in[21] ^ data_in[23] ^ data_in[24] ^ data_in[25] ^ data_in[26] ^ data_in[27] ^ data_in[29];
	assign	lfsr_c[28] = lfsr_q[2] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[8] ^ lfsr_q[12] ^ lfsr_q[20] ^ lfsr_q[21] ^ lfsr_q[22] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[26] ^ lfsr_q[27] ^ lfsr_q[28] ^ lfsr_q[30] ^ data_in[2] ^ data_in[5] ^ data_in[6] ^ data_in[8] ^ data_in[12] ^ data_in[20] ^ data_in[21] ^ data_in[22] ^ data_in[24] ^ data_in[25] ^ data_in[26] ^ data_in[27] ^ data_in[28] ^ data_in[30];
	assign	lfsr_c[29] = lfsr_q[3] ^ lfsr_q[6] ^ lfsr_q[7] ^ lfsr_q[9] ^ lfsr_q[13] ^ lfsr_q[21] ^ lfsr_q[22] ^ lfsr_q[23] ^ lfsr_q[25] ^ lfsr_q[26] ^ lfsr_q[27] ^ lfsr_q[28] ^ lfsr_q[29] ^ lfsr_q[31] ^ data_in[3] ^ data_in[6] ^ data_in[7] ^ data_in[9] ^ data_in[13] ^ data_in[21] ^ data_in[22] ^ data_in[23] ^ data_in[25] ^ data_in[26] ^ data_in[27] ^ data_in[28] ^ data_in[29] ^ data_in[31];
	assign	lfsr_c[30] = lfsr_q[4] ^ lfsr_q[7] ^ lfsr_q[8] ^ lfsr_q[10] ^ lfsr_q[14] ^ lfsr_q[22] ^ lfsr_q[23] ^ lfsr_q[24] ^ lfsr_q[26] ^ lfsr_q[27] ^ lfsr_q[28] ^ lfsr_q[29] ^ lfsr_q[30] ^ data_in[4] ^ data_in[7] ^ data_in[8] ^ data_in[10] ^ data_in[14] ^ data_in[22] ^ data_in[23] ^ data_in[24] ^ data_in[26] ^ data_in[27] ^ data_in[28] ^ data_in[29] ^ data_in[30];
	assign	lfsr_c[31] = lfsr_q[5] ^ lfsr_q[8] ^ lfsr_q[9] ^ lfsr_q[11] ^ lfsr_q[15] ^ lfsr_q[23] ^ lfsr_q[24] ^ lfsr_q[25] ^ lfsr_q[27] ^ lfsr_q[28] ^ lfsr_q[29] ^ lfsr_q[30] ^ lfsr_q[31] ^ data_in[5] ^ data_in[8] ^ data_in[9] ^ data_in[11] ^ data_in[15] ^ data_in[23] ^ data_in[24] ^ data_in[25] ^ data_in[27] ^ data_in[28] ^ data_in[29] ^ data_in[30] ^ data_in[31];

  always @(posedge clk, posedge rst) begin
    if(rst) begin
      lfsr_q <= {32{1'b1}};
    end
    else begin
      lfsr_q <= crc_en ? lfsr_c : lfsr_q;
    end
  end // always

endmodule // crc





