/********************************** (C) COPYRIGHT *******************************
* File Name          : vector_remap.c
* Author             : kingsley@wch
* Version            : V1.1
* Date               : 20210325
* Description 		 : vector remap for ch57x arm cortex-m0 series
*                      CH579 ble lib >= 1.44(EVT1.9)
                        
                       user bootloader flash start address must >= 0x200
                       for kings_ota user bootloader flash start address must >= 0x400
                       
                       bit7 of R8_GLOB_RESET_KEEP for the flag of this_user_boot and user_app
                            1 for this_user_boot
                            0 for user_app
*******************************************************************************/


#include "stdint.h"


#ifndef APP_START_ADDRESS
#define APP_START_ADDRESS 0x4000
#endif

extern char Load$$LR$$LR_IROM1$$Base;
uint32_t base_start =  ((uint32_t)&Load$$LR$$LR_IROM1$$Base);
const uint32_t vector_remap_firmwave[512/4] __attribute__((at(0x0))) = {
    0x20008000,0x00000169,0x000000bf,0x000000bf,
    0x00000000,0x00000000,0x00000000,0x00000000,
    0x00000000,0x00000000,0x00000000,0x000000bf,
    0x00000000,0x00000000,0x000000bf,0x000000bf,
    0x000000bf,0x000000bf,0x000000bf,0x000000bf,
    0x000000ed,0x000000bf,0x000000bf,0x000000bf,
    0x000000bf,0x000000bf,0x000000bf,0x000000bf,
    0x000000bf,0x000000bf,0x000000bf,0x000000bf,
    0x000000bf,0x000000bf,0x000000bf,0x000000bf,
    0x46854803,0xf876f000,0x47004800,0x0000013b,
    0x20008000,0x492b2057,0x20a87008,0x20887008,
    0x39404928,0x20008108,0x70084926,0xb5104770,
    0x79c04824,0x40082180,0xd0072800,0xf83ff000,
    0x49210080,0x580c6809,0xe00647a0,0xf837f000,
    0x491e0080,0x580c6809,0xbd1047a0,0x4819b510,
    0x218079c0,0x28004008,0xf000d007,0x0080f828,
    0x68094915,0x47a0580c,0xf000e006,0x0080f820,
    0x68094912,0x47a0580c,0xb570bd10,0x68204604,
    0x4008490f,0x07492101,0xd1054288,0x68206865,
    0x8808f380,0x47a8bf00,0x2000bd70,0x490943c0,
    0x48056008,0xf7ff6800,0xbf00ffe8,0xf3efe7fe,
    0x47708005,0x40001040,0x000001f0,0x000001f4,
    0x2ffe0000,0xe000e180,0x47804804,0x47004804,
    0xe7fee7fe,0xe7fee7fe,0x0000e7fe,0x000000a5,
    0x00000091,0x25014c06,0xe0054e06,0xcc0768e3,
    0x3c0c432b,0x34104798,0xd3f742b4,0xff7cf7ff,
    0x000001a8,0x000001a8,0x00000000,0x00000000,
    0x00000000,0x00000000,0x00000000,0x00000000,
    0x00000000,0x00000000,0x00000000,0x00000000,
    0x00000000,0x00000000,0x00000000,0x00000000,
    0x00000000,0x00000000,0x00000000,0x00000000,
    ((uint32_t)&Load$$LR$$LR_IROM1$$Base) , //user_boot address
    APP_START_ADDRESS,                      //user_app address
    0xffffffff, //res
    0xffffffff  //res
};
