/********************************** (C) COPYRIGHT *******************************
* File Name          : Main.c
* Author             : kingsley@wch
* Version            : V1.0
* Date               : 2021/7/09
* Description 		 : ch57x_user_bootloader
*******************************************************************************/

#include "CH57x_common.h"

void DebugInit(void)		
{
    GPIOA_SetBits(GPIO_Pin_9);
    GPIOA_ModeCfg(GPIO_Pin_8, GPIO_ModeIN_PU);
    GPIOA_ModeCfg(GPIO_Pin_9, GPIO_ModeOut_PP_5mA);
    UART1_DefInit();
}

typedef  void (*app_t)(void);

void iap_load_app(uint32_t appxaddr) {
    app_t jump2app;
   
    //disable all interrupt
    //__disable_irq();
    NVIC->ICER[0] = 0xffffffff;
    NVIC->ICPR[0] = 0xffffffff;

    //for ch579 interrupt vector remap flag
    //R8_GLOB_RESET_KEEP bit701  for user app interrupt 
    R8_GLOB_RESET_KEEP &= ~0x80;
    
    jump2app=(app_t)*(__IO uint32_t*)(appxaddr+4);//用户代码区第二个字为程序开始地址(复位地址)
    __set_MSP(*(__IO uint32_t*)appxaddr);//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
    jump2app();//跳转到APP.

}

int main()
{     
    //R8_GLOB_RESET_KEEP bit7=1  for this bootloader interrupt 
    //R8_GLOB_RESET_KEEP bit7=0  for user app interrupt 
    R8_GLOB_RESET_KEEP |= 0x80;
    /* 配置串口调试 */   
    DebugInit();
    PRINT("user bootloader enter .....\r\n");

//for interrupt test    
#if 1       
    TMR0_TimerInit( FREQ_SYS/10 );                  // 设置定时时间 100ms
    TMR0_ITCfg(ENABLE, TMR0_3_IT_CYC_END);          // 开启中断
    NVIC_EnableIRQ( TMR0_IRQn );
    
#endif 

    DelayMs(2000);
    PRINT("jump to user app at 0x4000\r\n");
    //wating for print fifo end
    DelayMs(5);
    iap_load_app(0x4000);
    while(1);    
}



void TMR0_IRQHandler( void )        // TMR0 定时中断
{
    if( TMR0_GetITFlag( TMR0_3_IT_CYC_END ) )
    {
        TMR0_ClearITFlag( TMR0_3_IT_CYC_END );      // 清除中断标志
        
        PRINT("user boot interrupt \r\n"); 
    }
}






