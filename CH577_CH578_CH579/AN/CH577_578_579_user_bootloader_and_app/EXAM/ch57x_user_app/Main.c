/********************************** (C) COPYRIGHT *******************************
* File Name          : Main.c
* Author             : kingsley@wch
* Version            : V1.0
* Date               : 2021/7/09
* Description 		 : ch57x_user_app
*******************************************************************************/

#include "CH57x_common.h"

void DebugInit(void)		
{
    GPIOA_SetBits(GPIO_Pin_9);
    GPIOA_ModeCfg(GPIO_Pin_8, GPIO_ModeIN_PU);
    GPIOA_ModeCfg(GPIO_Pin_9, GPIO_ModeOut_PP_5mA);
    UART1_DefInit();
}

int main() {     

  
/* 配置串口调试 */   
    DebugInit();
    PRINT("user app enter .....\r\n");

   
//for interrupt testing    
#if 1         
    TMR0_TimerInit( FREQ_SYS/10 );                  // 设置定时时间 100ms
    TMR0_ITCfg(ENABLE, TMR0_3_IT_CYC_END);          // 开启中断
    NVIC_EnableIRQ( TMR0_IRQn );
    
#endif 
    while(1);    
}



void TMR0_IRQHandler( void )                        // TMR0 定时中断
{
    if( TMR0_GetITFlag( TMR0_3_IT_CYC_END ) )
    {
        TMR0_ClearITFlag( TMR0_3_IT_CYC_END );      // 清除中断标志
        PRINT("user app interrupt \r\n");
    }
}




