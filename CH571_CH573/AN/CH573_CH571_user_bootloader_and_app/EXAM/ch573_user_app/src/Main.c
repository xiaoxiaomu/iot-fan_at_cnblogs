/********************************** (C) COPYRIGHT *******************************
 * File Name          : Main.c
 * Author             : kingsley@wch
 * Version            : V1.0
 * Date               : 2021/03/06
 * Description        : ch573/571 user bootloader
 *******************************************************************************/

#include "CH57x_common.h"



void DebugInit( void ) {
    GPIOA_SetBits( GPIO_Pin_9 );
    GPIOA_ModeCfg( GPIO_Pin_8, GPIO_ModeIN_PU );
    GPIOA_ModeCfg( GPIO_Pin_9, GPIO_ModeOut_PP_5mA );
    UART1_DefInit();
}

int main() {
    UINT8 i;

    SetSysClock( CLK_SOURCE_PLL_60MHz );

    /* 配置串口调试 */
    DebugInit();
    PRINT( "Start @ChipID=%02X\n", R8_CHIP_ID );
    PRINT("user app start...... \r\n");

#if 1       /* 定时器0，设定100ms定时器进行IO口闪灯， PB15-LED */

    GPIOB_SetBits( GPIO_Pin_15 );
    GPIOB_ModeCfg( GPIO_Pin_15, GPIO_ModeOut_PP_5mA );

    TMR0_TimerInit( FREQ_SYS / 10 );                  // 设置定时时间 100ms
    TMR0_ITCfg( ENABLE, TMR0_3_IT_CYC_END );          // 开启中断
    PFIC_EnableIRQ( TMR0_IRQn );

#endif


    DelayMs(2000);
    //PRINT("start load app... \r\n");
    DelayMs(5);
    //iap_load_app(0x4000);
    while( 1 )
        ;
}

__attribute__((interrupt("WCH-Interrupt-fast")))
__attribute__((section(".highcode")))
void TMR0_IRQHandler( void ) {      // TMR0 定时中断
    if ( TMR0_GetITFlag( TMR0_3_IT_CYC_END ) ) {
        TMR0_ClearITFlag( TMR0_3_IT_CYC_END );      // 清除中断标志
        GPIOB_InverseBits( GPIO_Pin_15 );
    }
    PRINT("app interrupt test \r\n");
}



