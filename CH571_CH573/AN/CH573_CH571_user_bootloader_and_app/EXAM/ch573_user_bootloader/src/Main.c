/********************************** (C) COPYRIGHT *******************************
 * File Name          : Main.c
 * Author             : kingsley@wch
 * Version            : V1.0
 * Date               : 2021/03/06
 * Description        : ch573/571 user bootloader
 *******************************************************************************/

#include "CH57x_common.h"


UINT8V capFlag = 0;

#define CH573

#if defined(CH573)

typedef  void (*app_t)(void);

static volatile uint8_t switch_to_machine_mode = 0x00;
static volatile uint32_t jump_app_addr = 0;
void ch573_switch_to_machine_mode(void) {
    switch_to_machine_mode = 0x01;
    PFIC_EnableIRQ(SWI_IRQn);
    PFIC_SetPendingIRQ(SWI_IRQn);
    while (switch_to_machine_mode == 0x01);
}

__attribute__((interrupt("WCH-Interrupt-fast")))
__attribute__((section(".highcode")))
void SW_Handler(void) {
    PRINT("SW handler\r\n");
    app_t jump2app;
    if (switch_to_machine_mode == 0x01) {
        //asm("li t0, 0x1888");
        PFIC->IRER[0] = 0xffffffff;
        PFIC->IRER[1] = 0xffffffff;
        jump2app = (app_t) (__IO uint32_t*) (jump_app_addr);
        jump2app();
    }
}

void iap_load_app(uint32_t appxaddr) {
#if defined(CH579)
    app_t jump2app;
    if(((*(__IO uint32_t*)appxaddr)&0x2FFE0000)==0x20000000) { //检查栈顶地址是否合法.
        //disable all interrupt
        NVIC->ICER[0] = 0xffffffff;
        //for ch579 interrupt vector remap flag
        R8_GLOB_RESET_KEEP &= ~0x80;

        jump2app=(app_t)*(__IO uint32_t*)(appxaddr+4);//用户代码区第二个字为程序开始地址(复位地址)
        __set_MSP(*(__IO uint32_t*)appxaddr);//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
        jump2app();//跳转到APP.
    }
#elif defined(CH573)
    jump_app_addr = appxaddr;
    //switch to machine mode
    ch573_switch_to_machine_mode();
    //disable all interrupt here
    //jumpApp();
    //#error "you must define ch579 or ch573 first!"
#elif defined(CH583)
#error "CH583 is not supported now!"
#else
#error "you must define ch579 or ch573 first!"
#endif
}
#endif


void DebugInit( void ) {
    GPIOA_SetBits( GPIO_Pin_9 );
    GPIOA_ModeCfg( GPIO_Pin_8, GPIO_ModeIN_PU );
    GPIOA_ModeCfg( GPIO_Pin_9, GPIO_ModeOut_PP_5mA );
    UART1_DefInit();
}

int main() {
    UINT8 i;

    SetSysClock( CLK_SOURCE_PLL_60MHz );

    /* 配置串口调试 */
    DebugInit();
    PRINT( "Start @ChipID=%02X\n", R8_CHIP_ID );
    PRINT("user bootloader start...... \r\n");

#if 1       /* 定时器0，设定100ms定时器进行IO口闪灯， PB15-LED */

    GPIOB_SetBits( GPIO_Pin_15 );
    GPIOB_ModeCfg( GPIO_Pin_15, GPIO_ModeOut_PP_5mA );

    TMR0_TimerInit( FREQ_SYS / 10 );                  // 设置定时时间 100ms
    TMR0_ITCfg( ENABLE, TMR0_3_IT_CYC_END );          // 开启中断
    PFIC_EnableIRQ( TMR0_IRQn );

#endif


    DelayMs(2000);
    PRINT("start load app ....\r\n");
    DelayMs(5);

    iap_load_app(0x4000);
    while( 1 )
        ;
}

__attribute__((interrupt("WCH-Interrupt-fast")))
__attribute__((section(".highcode")))
void TMR0_IRQHandler( void ) {      // TMR0 定时中断
    if ( TMR0_GetITFlag( TMR0_3_IT_CYC_END ) ) {
        TMR0_ClearITFlag( TMR0_3_IT_CYC_END );      // 清除中断标志
        GPIOB_InverseBits( GPIO_Pin_15 );
    }
    PRINT("boot interrupt test \r\n");
}



